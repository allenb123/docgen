# `docgen`

A super-simple documentation generator for C.

## Usage
### Commenting code
Functions, types, macros, and variables will be documented if there is a comment that begins with `/**` on the previous line.

### Generating documentation
#### Markdown
```bash
cat in.h | doc-gen > out.md
```

#### HTML
```bash
cat in.h | doc-gen | markdown -f +fencedcode > out.html
```

## Building
```bash
go build -o doc-gen main.go
```
