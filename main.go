package main

import (
  "fmt"
  "io/ioutil"
  "os"
  "./docgen"
)

func main() {
	str, err := ioutil.ReadAll(os.Stdin)
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error:", err)
		os.Exit(1)
	}
	fmt.Println(docgen.Generate(string(str), docgen.C))
}
