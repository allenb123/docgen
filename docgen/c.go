package docgen

import "regexp"

const C_Type = "(const\\s+)?((struct|union|enum)\\s+)?[A-Za-z0-9_]+\\*?"
const C_Modifier = "(static|extern|inline)"

var C = Language {
	"Comment": regexp.MustCompile("(?s)/\\*\\*\\s*(?P<content>.+?)\\s*\\*/"),
	"Functions": regexp.MustCompile("(" + C_Modifier + "\\s+)*" + C_Type + "\\s+(?P<name>[A-Za-z0-9_]+)\\s*\\(.*\\)\\s*"),
	"Variables": regexp.MustCompile("(?m)^\\s*(" + C_Modifier + "\\s+)*" + C_Type + "\\s+(?P<name>[A-Za-z0-9_]+)(\\s*=\\s*[^;]*)?\\s*;"),
	"Types": regexp.MustCompile("(?s)(typedef\\s+" + C_Type + "(\\s*{.*?})?\\s*(?P<name>[A-Za-z0-9_]+)|(?P<name>(struct|enum|union)\\s+[A-Za-z0-9_]+)(\\s*\\{.*?\\})?)\\s*;"),
	"Macros": regexp.MustCompile("#define (?P<name>[A-Za-z0-9_]+)(\\s*\\([^()]*?\\))?"),
}
