package docgen

import (
	"regexp"
	"strings"
	"sort"
)

type Language map[string]*regexp.Regexp

func Generate(in string, lang Language) string {
	out := ""

	names := make([]string, len(lang))
	i := 0
	for name, _ := range lang {
		names[i] = name
		i++
	}
	sort.Strings(names)

	for _, name := range names {
		re := lang[name]
		if name == "Comment" {
			continue
		}
		
		all := re.FindAllStringIndex(in, -1)
		if len(all) != 0 {
			out += "# " + name + "\n"
		}
		// Loop through all function strings
		for _, pair := range all {
			signature := in[pair[0]:pair[1]]

			var comment string
			if cre := lang["Comment"]; cre != nil {
				index := strings.LastIndex(in[:pair[0]], "\n")
				if index == -1 { continue }
				s := cre.FindAllStringSubmatchIndex(in[:index], -1)
				if len(s) != 0 && s[len(s) - 1][1] == index {
					comment_bytes := cre.ExpandString(nil, "$content", in[:index], s[len(s) - 1])
					comment = strings.Trim(string(comment_bytes), " \n\t")
				} else { // if no comment, continue
					continue
				}
			}

			
			name := re.ReplaceAllString(signature, "$name")
			out += "## `" + name + "`\n"
			out += "```\n"
			out += signature + "\n"
			out += "```\n"
			out += comment + "\n"
		}
	}
	
	return out
}
